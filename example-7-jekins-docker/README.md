
#### https://github.com/4OH4/jenkins-docker


```

docker build -t jenkins-docker .

docker run -it -p 8082:8080 -d \
    -v /var/jenkins_home3:/var/jenkins_home \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --restart unless-stopped \
    jenkins-docker

```